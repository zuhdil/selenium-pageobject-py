import pytest
from unittest.mock import Mock, call

from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

from pageobject.find import FindBy, FindAllBy
from pageobject.pageobject import PageObject, WrapElement


@pytest.fixture
def webdriver():
    return Mock(WebDriver)


@pytest.fixture
def webelement():
    return Mock(WebElement)


class FakeWrap(WrapElement):
    pass


class FakePage(PageObject):
    url = 'http://localhost'
    by_css = FindBy(css='foo')
    all_by_css = FindAllBy(css='bar')
    wrap = FindBy(css='wrap', wrap=FakeWrap)
    wraps = FindAllBy(FakeWrap, css='wraps')


class Test_FindBy:
    def test_no_locator_error(self):
        with pytest.raises(ValueError):
            FindBy()

    def test_ambiguous_locator_error(self):
        with pytest.raises(ValueError):
            FindBy(id='id', name='foo')

    def test_element_invalid_locator(self):
        with pytest.raises(ValueError):
            FindBy(invalid='foo')

    def test_wrong_wrap_error(self):
        with pytest.raises(ValueError):
            FindBy(tag_name='tag', wrap=Mock)

    def test_get_descriptor(self, webdriver):
        page = FakePage(webdriver)
        webdriver.find_element.return_value = 'XXX'
        assert page.by_css == 'XXX'
        assert webdriver.find_element.mock_calls == [
                call(By.CSS_SELECTOR, 'foo')]

    def test_get_no_instance(self):
        assert isinstance(FakePage.by_css, FindBy)
        assert FakePage.by_css.locator == (By.CSS_SELECTOR, 'foo')

    def test_get_not_found(self, webdriver):
        page = FakePage(webdriver)
        webdriver.find_element.side_effect = NoSuchElementException
        assert page.by_css is None

    def test_get_wrap_element(self, webdriver, webelement):
        page = FakePage(webdriver)
        webdriver.find_element.return_value = webelement
        assert isinstance(page.wrap, FakeWrap)


class Test_FindAllBy:
    def test_no_locator_error(self):
        with pytest.raises(ValueError):
            FindAllBy()

    def test_ambiguous_locator_error(self):
        with pytest.raises(ValueError):
            FindAllBy(id='id', name='foo')

    def test_element_invalid_locator(self):
        with pytest.raises(ValueError):
            FindAllBy(invalid='foo')

    def test_wrong_wrap_error(self):
        with pytest.raises(ValueError):
            FindAllBy(tag_name='tag', wrap=Mock)

    def test_get_no_instance(self):
        assert isinstance(FakePage.all_by_css, FindAllBy)
        assert FakePage.all_by_css.locator == (By.CSS_SELECTOR, 'bar')

    def test_get_descriptor(self, webdriver):
        page = FakePage(webdriver)
        webdriver.find_elements.return_value = ['XXX', 'YYY']
        assert page.all_by_css == ['XXX', 'YYY']
        assert webdriver.find_elements.mock_calls == [
                call(By.CSS_SELECTOR, 'bar')]

    def test_get_wrap_element(self, webdriver, webelement):
        page = FakePage(webdriver)
        webdriver.find_elements.return_value = [webelement]
        assert isinstance(page.wraps[0], FakeWrap)
