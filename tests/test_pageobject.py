import pytest
from unittest.mock import Mock, call

from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement

from pageobject.pageobject import PageObject, WrapElement


class FakePage(PageObject):
    url = 'http://localhost'


class DummyPage(PageObject):
    pass


@pytest.fixture
def webdriver():
    return Mock(WebDriver)


@pytest.fixture
def webelement():
    return Mock(WebElement)


class Test_PageObject:
    def test_url_empty(self):
        page = DummyPage(None)
        assert page.url is None

    def test_url_parameter(self):
        url = 'http://localhost'
        page = DummyPage(None, url)
        assert page.url == url

    def test_url_from_class_attribute(self):
        page = FakePage(None)
        assert page.url == 'http://localhost'

    def test_open(self, webdriver):
        page = FakePage(webdriver)
        next = page.open()
        assert next is page
        assert webdriver.get.mock_calls == [call('http://localhost')]

    def test_goto(self, webdriver):
        page = FakePage(webdriver, 'http://localhost/foo')
        next = page.goto('/bar')
        assert next is not page
        assert isinstance(next, PageObject)
        assert webdriver.get.mock_calls == [call('http://localhost/bar')]
        assert next.url == 'http://localhost/bar'


class Test_WrapElement:
    def test_init_error(self):
        with pytest.raises(ValueError):
            WrapElement(Mock())

    def test_proxy_element(self, webelement):
        wrap = WrapElement(webelement)
        wrap.get_attribute('name')
        assert webelement.get_attribute.called is True
