from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from .pageobject import WrapElement


LOCATOR_MAP = {
        'css': By.CSS_SELECTOR,
        'id': By.ID,
        'name': By.NAME,
        'xpath': By.XPATH,
        'link_text': By.LINK_TEXT,
        'partial_link_text': By.PARTIAL_LINK_TEXT,
        'tag_name': By.TAG_NAME,
        'class_name': By.CLASS_NAME}


class FindBy:
    def __init__(self, wrap=None, **kwargs):
        if not kwargs:
            raise ValueError('Please specify a locator')
        if len(kwargs) > 1:
            raise ValueError('Please specify only one locator')
        if wrap and not issubclass(wrap, WrapElement):
            raise ValueError('wrap types should inherit WrapElement')

        k, v = next(iter(kwargs.items()))

        if k not in LOCATOR_MAP:
            raise ValueError('Invalid locator "{}"'.format(k))

        self.wrap = wrap
        self.locator = (LOCATOR_MAP[k], v)

    def __get__(self, obj, objtype):
        if not obj:
            return self

        return self._find(obj)

    def _find(self, context):
        try:
            element = context.find_element(*self.locator)
            return element if not self.wrap else self.wrap(element)
        except NoSuchElementException:
            return None


class FindAllBy(FindBy):
    def _find(self, context):
        els = context.find_elements(*self.locator)
        return els if not self.wrap else [self.wrap(el) for el in els]
