from urllib.parse import urljoin
from selenium.webdriver.remote.webelement import WebElement

class PageObject:
    url = None

    def __init__(self, driver, url=None):
        self.driver = driver
        self.url = url if url else self.url

    def open(self):
        self.driver.get(self.url)
        return self

    def goto(self, path):
        url = urljoin(self.url, path)
        self.driver.get(url)
        return PageObject(self.driver, url)

    def find_element(self, *args):
        return self.driver.find_element(*args)

    def find_elements(self, *args):
        return self.driver.find_elements(*args)


class WrapElement:
    def __init__(self, element):
        if not isinstance(element, WebElement):
            raise ValueError('element must be instance of WebElement')

        self._inner = element

    def __getattr__(self, attr):
        return getattr(self._inner, attr)
