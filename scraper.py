import pickle
import json
from os.path import isfile
from datetime import datetime
from urllib.parse import urlencode, urljoin
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import (
        text_to_be_present_in_element, visibility_of_element_located)
from pageobject.pageobject import PageObject, WrapElement
from pageobject.find import FindBy, FindAllBy


def chrome_driver():
    options = webdriver.ChromeOptions()
    options.add_argument('--incognito')
    options.add_argument('--disable-notifications')
    return webdriver.Chrome(chrome_options=options)


class LoginFailureError(RuntimeError):
    pass


class LoginPage(PageObject):
    email = FindBy(id='email')
    password = FindBy(id='pass')
    login_btn = FindBy(css='#login_form input[type=submit]')
    logged_in = FindBy(css='input[name=q]')

    def __init__(self, driver, url, cookies_file='cookies.pkl'):
        super().__init__(driver, url)
        self.cookies_file = cookies_file
        self.wait = WebDriverWait(self.driver, 10)

    def load_session(self, email, password):
        if not isfile(self.cookies_file):
            self.login(email, password)
        else:
            cookies = pickle.load(open(self.cookies_file, 'rb'))
            for cookie in cookies:
                self.driver.add_cookie(cookie)

            self.open()

        if not self.is_logged_in():
            self.force_login(email, password)

        if not self.is_logged_in():
            raise LoginFailureError(email)

        return HomePage(self.driver, self.driver.current_url)

    def is_logged_in(self):
        return self.logged_in is not None

    def login(self, email, password):
        self.wait_form_ready()
        self.email.send_keys(email)
        self.password.send_keys(password)
        self.login_btn.click()

        if self.is_logged_in():
            pickle.dump(self.driver.get_cookies(), open(self.cookies_file, 'wb'))

    def force_login(self, email, password):
        counter = 0
        while not self.is_logged_in() and counter < 3:
            counter = counter + 1
            self.driver.delete_all_cookies()
            self.open()
            self.login(email, password)

    def wait_form_ready(self):
        self.wait.until(visibility_of_element_located(
            type(self).login_btn.locator))


class HomePage(PageObject):

    def search(self, query, start, end):
        filter = json.dumps({
                    'start_month': start.strftime('%Y-%m'),
                    'end_month': end.strftime('%Y-%m')},
                separators=(',', ':'))
        query_str = urlencode({'q': query, 'filters_rp_creation_time': filter})
        search_url = urljoin(self.url, '/search/top/?{}'.format(query_str))

        return SearchResultsPage(self.driver, search_url).open()


class ResultContent(WrapElement):
    title_container = FindBy(tag_name='h5')
    time_container = FindBy(css='h5+* a>abbr')
    snippet_container = FindBy(css='.userContent')

    def __init__(self, element):
        super().__init__(element)
        self._title = None
        self._datetime = None
        self._url = None
        self._snippet = None

    @property
    def title(self):
        return self._title if self._title else self.title_container.text

    @property
    def datetime(self):
        if not self._datetime:
            self._datetime = datetime.fromtimestamp(
                    int(self.time_container.get_attribute('data-utime')))

        return self._datetime

    @property
    def url(self):
        if not self._url:
            link_el = self.time_container.get_property('parentElement')
            self._url = link_el.get_attribute('href')

        return self._url

    @property
    def snippet(self):
        return self._snippet if self._snippet else self.snippet_container.text

    def to_dict(self):
        return {
                'title': self.title,
                'datetime': self.datetime,
                'url': self.url,
                'snippet': self.snippet}


class ResultArea(WrapElement):
    contents = FindAllBy(ResultContent, css='.userContentWrapper')


class SearchResultsPage(PageObject):
    result_area = FindBy(ResultArea, id='browse_result_area')
    end_of_results = FindBy(id='browse_end_of_results_footer')

    def __init__(self, driver, url):
        super().__init__(driver, url)
        self.wait = WebDriverWait(self.driver, 10)

    def open(self):
        super().open()

        counter = 0
        scroll_script = 'window.scrollTo(0, document.body.scrollHeight);'
        while not self.end_of_results and counter < 10:
            self.wait_results_loaded()
            counter = counter + 1
            self.driver.execute_script(scroll_script)

        return self

    def wait_results_loaded(self):
        self.wait.until_not(text_to_be_present_in_element(
            type(self).result_area.locator, 'Loading more results...'))

    def get_results(self):
        for content in self.result_area.contents:
            yield content.to_dict()



def testrun():
    # search_term = 'line 6 pod hd500x'
    search_term = 'boss gt-100'
    driver = chrome_driver()
    try:
        login = LoginPage(driver, 'http://www.facebook.com')
        home = login.open().load_session('zuhdil@yahoo.com', 'telolet')
        search = home.search(search_term, datetime.now(), datetime.now())

        for result in search.get_results():
            print('{title} ({datetime})\n<{url}>\n{snippet}\n\n'.format(**result))
    finally:
        driver.quit()


if __name__ == '__main__':
    testrun()
